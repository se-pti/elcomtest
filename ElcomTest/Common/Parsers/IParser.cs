﻿using System.Collections.Generic;

namespace Common
{
    /// <summary>
    /// Интерфейс предназначен для выводя значения поля в документе.
    /// Так как десериализация довольно сложный процесс и так как типы файла могут быть разнами выбрано решение именно парса документов.
    /// </summary>
    /// <typeparam name="TFileType">Тип документа</typeparam>
    public interface IParser<TFileType>
    {
        /// <summary>
        /// Вывод значения поля, которое является коллекцией. Т.е. вывод значения дочерних полей для указанного поля.
        /// </summary>
        /// <param name="file">файл</param>
        /// <param name="rowName">имя необходимого поля</param>
        IEnumerable<string> FiendAllRowsInXmlDocByCollectionRows(TFileType file, string rowName);

        /// <summary>
        /// Вывод значения поля, которое является единственным, т.е. у поля нет дочерних полей.
        /// </summary>
        /// <param name="file">файл</param>
        /// <param name="rowName">имя необходимого поля</param>
        IEnumerable<string> FiendAllRowsInXmlDocBySingleRows(TFileType file, string rowName);
    }
}
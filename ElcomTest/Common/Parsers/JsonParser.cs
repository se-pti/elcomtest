﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Common
{
    public class JsonParser : IParser<JObject>
    {
        public IEnumerable<string> FiendAllRowsInXmlDocByCollectionRows(JObject file, string rowName)
        {
            if (file == null || String.IsNullOrEmpty(rowName))
            {
                return null;
            }

            var result = new List<string>();
            var rows = file.SelectTokens(rowName);
            foreach (var row in rows)
            {
                foreach (var item in row)
                {
                    result.Add(item.Value<string>());
                }
            }

            return result;
        }

        public IEnumerable<string> FiendAllRowsInXmlDocBySingleRows(JObject file, string rowName)
        {
            if (file == null || String.IsNullOrEmpty(rowName))
            {
                return null;
            }

            var result = new List<string>();
            var rows = file.SelectTokens(rowName);
            foreach (var row in rows)
            {
                result.Add(row.ToString());
            }

            return result;
        }
    }
}

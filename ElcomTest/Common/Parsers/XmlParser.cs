﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace Common
{
    public class XmlParser : IParser<XmlDocument>
    {
        public IEnumerable<string> FiendAllRowsInXmlDocByCollectionRows(XmlDocument file, string rowName)
        {
            if (file == null || String.IsNullOrEmpty(rowName))
            {
                return null;
            }

            var result = new List<string>();
            var rows = file.GetElementsByTagName(rowName);
            for (int i = 0; i < rows.Count; i++)
            {
                foreach (XmlNode item in rows[i].ChildNodes)
                {
                    result.Add(item.InnerText);
                }
            }

            return result;
        }

        public IEnumerable<string> FiendAllRowsInXmlDocBySingleRows(XmlDocument file, string rowName)
        {
            if (file == null || String.IsNullOrEmpty(rowName))
            {
                return null;
            }

            var result = new List<string>();
            var rows = file.GetElementsByTagName(rowName);
            for (int i = 0; i < rows.Count; i++)
            {
                result.Add(rows[i].InnerText);
            }

            return result;
        }
    }
}

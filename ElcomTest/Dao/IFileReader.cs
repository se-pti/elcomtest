﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Dao
{
    public interface IFileReader
    {
        /// <summary>
        /// Выдать список файлов из папки
        /// </summary>
        /// <param name="path">Путь к папке с файлами</param>
        Task<IEnumerable<string>> GetPaths(string path, CancellationToken cancellationToken = default);

        /// <summary>
        /// Выдать значение полей из файлов
        /// </summary>
        /// <param name="paths">Пути до файлов</param>
        /// <param name="rowName">Имя поля</param>
        Task<IEnumerable<string>> GetRowsFromFiles(IEnumerable<string> paths, string rowName, bool isCollectionRow, CancellationToken cancellationToken = default);
    }
}
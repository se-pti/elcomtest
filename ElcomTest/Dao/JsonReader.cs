﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Newtonsoft.Json.Linq;

namespace Dao
{
    public class JsonReader : LoaderBase, IFileReader
    {
        private const string _extension = ".json";
        private readonly IParser<JObject> _parser;

        public JsonReader(IParser<JObject> parser)
        {
            _parser = parser;
        }

        public Task<IEnumerable<string>> GetPaths(string path, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                if (String.IsNullOrEmpty(path))
                {
                    return null;
                }

                return GetFilesNameInPathByExtension(path, _extension, cancellationToken);
            });
        }

        public Task<IEnumerable<string>> GetRowsFromFiles(IEnumerable<string> paths, string rowName, bool isCollectionRow, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                try
                {
                    if (paths.IsNullOrEmpty()|| String.IsNullOrEmpty(rowName))
                    {
                        return null;
                    }

                    var result = new List<string>();
                    foreach (var path in paths)
                    {
                        cancellationToken.ThrowIfCancellationRequested();

                        var doc = new JObject();
                        using (var reader = new StreamReader(path))
                        {
                            doc = JObject.Parse(reader.ReadToEnd());
                        }

                        result.AddRange(isCollectionRow
                            ? _parser.FiendAllRowsInXmlDocByCollectionRows(doc, rowName)
                            : _parser.FiendAllRowsInXmlDocBySingleRows(doc, rowName));
                    }

                    return (IEnumerable<string>)result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            });
        }
    }
}

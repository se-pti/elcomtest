﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dao
{
    public abstract class LoaderBase
    {
        protected IEnumerable<string> GetFilesNameInPathByExtension(string path, string extensions, CancellationToken cancellationToken = default)
        {
            if (!path.Last().Equals('\\'))
            {
                path = path + @"\";
            }

            var dir = new DirectoryInfo(path);

            var paths = new List<string>();
            foreach (var item in dir.GetFiles())
            {
                cancellationToken.ThrowIfCancellationRequested();
                if (item.Extension.Equals(extensions))
                {
                    paths.Add(item.FullName);
                }
            }

            return paths;
        }
    }
}

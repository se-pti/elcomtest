﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using Common;

namespace Dao
{
    public class XmlReader : LoaderBase, IFileReader
    {
        private const string _extension = ".xml";
        private readonly IParser<XmlDocument> _parser;

        public XmlReader(IParser<XmlDocument> parser)
        {
            _parser = parser;
        }

        public Task<IEnumerable<string>> GetPaths(string path, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                if (String.IsNullOrEmpty(path))
                {
                    return null;
                }

                return GetFilesNameInPathByExtension(path, _extension, cancellationToken);
            });
        }

        public Task<IEnumerable<string>> GetRowsFromFiles(IEnumerable<string> paths, string rowName, bool isCollectionRow, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                try
                {
                    if (paths.IsNullOrEmpty() || String.IsNullOrEmpty(rowName))
                    {
                        return null;
                    }

                    var result = new List<string>();
                    foreach (var path in paths)
                    {
                        cancellationToken.ThrowIfCancellationRequested();

                        var doc = new XmlDocument();
                        using (var reader = new StreamReader(path))
                        {
                            doc.Load(reader);
                        }

                        result.AddRange(isCollectionRow
                            ? _parser.FiendAllRowsInXmlDocByCollectionRows(doc, rowName)
                            : _parser.FiendAllRowsInXmlDocBySingleRows(doc, rowName));
                    }

                    return (IEnumerable<string>)result;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return null;
                }
            });

        }
        
    }
}

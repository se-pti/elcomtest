﻿
namespace ElcomTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.radioButtonIsCollection = new System.Windows.Forms.RadioButton();
            this.radioButtonIsSingle = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRowName = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.richTextBoxResult = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь до файлов";
            // 
            // textBoxPath
            // 
            this.textBoxPath.Location = new System.Drawing.Point(149, 49);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(231, 20);
            this.textBoxPath.TabIndex = 1;
            // 
            // radioButtonIsCollection
            // 
            this.radioButtonIsCollection.AutoSize = true;
            this.radioButtonIsCollection.Checked = true;
            this.radioButtonIsCollection.Location = new System.Drawing.Point(40, 138);
            this.radioButtonIsCollection.Name = "radioButtonIsCollection";
            this.radioButtonIsCollection.Size = new System.Drawing.Size(136, 17);
            this.radioButtonIsCollection.TabIndex = 2;
            this.radioButtonIsCollection.TabStop = true;
            this.radioButtonIsCollection.Text = "Все значения из поля";
            this.radioButtonIsCollection.UseVisualStyleBackColor = true;
            // 
            // radioButtonIsSingle
            // 
            this.radioButtonIsSingle.AutoSize = true;
            this.radioButtonIsSingle.Location = new System.Drawing.Point(226, 138);
            this.radioButtonIsSingle.Name = "radioButtonIsSingle";
            this.radioButtonIsSingle.Size = new System.Drawing.Size(100, 17);
            this.radioButtonIsSingle.TabIndex = 3;
            this.radioButtonIsSingle.Text = "Значение поля";
            this.radioButtonIsSingle.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя поля";
            // 
            // textBoxRowName
            // 
            this.textBoxRowName.Location = new System.Drawing.Point(149, 95);
            this.textBoxRowName.Name = "textBoxRowName";
            this.textBoxRowName.Size = new System.Drawing.Size(231, 20);
            this.textBoxRowName.TabIndex = 5;
            this.textBoxRowName.Text = "Values";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(43, 180);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(41, 13);
            this.labelStatus.TabIndex = 6;
            this.labelStatus.Text = "Статус";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(75, 209);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 7;
            this.buttonStart.Text = "Запустить";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Location = new System.Drawing.Point(273, 209);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 8;
            this.buttonStop.Text = "Остановить";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // richTextBoxResult
            // 
            this.richTextBoxResult.Location = new System.Drawing.Point(40, 250);
            this.richTextBoxResult.Name = "richTextBoxResult";
            this.richTextBoxResult.Size = new System.Drawing.Size(340, 125);
            this.richTextBoxResult.TabIndex = 9;
            this.richTextBoxResult.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 400);
            this.Controls.Add(this.richTextBoxResult);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.textBoxRowName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.radioButtonIsSingle);
            this.Controls.Add(this.radioButtonIsCollection);
            this.Controls.Add(this.textBoxPath);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Элком+. Тест";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.RadioButton radioButtonIsCollection;
        private System.Windows.Forms.RadioButton radioButtonIsSingle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxRowName;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.RichTextBox richTextBoxResult;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autofac;
using Common;
using Logic;

namespace ElcomTest
{
    public partial class Form1 : Form
    {
        private LogicManager _logicManager;
        private CancellationTokenSource _tokenSource;

        public Form1()
        {
            InitializeComponent();
            var container = Setup.RegisterTypes();
            _logicManager = container.Resolve<LogicManager>();
            _tokenSource = new CancellationTokenSource();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(radioButtonIsCollection, "Читает содержимое поля как массив.");
            toolTip1.SetToolTip(radioButtonIsSingle, "Читает содержимое поля как единственное значене.");
        }

        private async void buttonStart_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxPath.Text))
            {
                MessageBox.Show("Укажите путь до файлов.");
                return;
            }

            if (String.IsNullOrEmpty(textBoxRowName.Text))
            {
                MessageBox.Show("Укажите имя поля.");
                return;
            }

            buttonStart.Enabled = false;
            buttonStop.Enabled = true;
            var result = new StringBuilder();

            try
            {
                var rows = await _logicManager.GetValuesLengthAsync(
                    textBoxPath.Text,
                    textBoxRowName.Text,
                    radioButtonIsCollection.Checked,
                    _tokenSource.Token
                );

                if (rows.IsNullOrEmpty())
                {
                    MessageBox.Show("Искомое поле не было найдено.");
                    buttonStart.Enabled = true;
                    buttonStop.Enabled = false;
                    return;
                }

                result.AppendLine($"Самое частое значение: {rows.OrderByDescending(x => x.Value).First().Key}.");
                foreach (var row in rows)
                {
                    result.AppendLine($"{row.Key} - {row.Value}");
                }
            }
            catch (TaskCanceledException)
            {
                MessageBox.Show("Поиск был завершен.");
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                return;
            }


            richTextBoxResult.Text = result.ToString();
            MessageBox.Show("Поиск завершен.");
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            _tokenSource.Cancel();
        }
    }
}

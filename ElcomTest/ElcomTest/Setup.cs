﻿using System.Xml;
using Autofac;
using Common;
using Dao;
using Logic;
using Newtonsoft.Json.Linq;
using XmlReader = Dao.XmlReader;

namespace ElcomTest
{
    public static class Setup
    {
        public static IContainer RegisterTypes()
        {
            var builder = new ContainerBuilder();

            builder.RegisterParsers();
            builder.RegisterDao();
            builder.RegisterLogic();

            return builder.Build();
        }

        private static ContainerBuilder RegisterParsers(this ContainerBuilder builder)
        {
            builder.RegisterType<XmlParser>().As<IParser<XmlDocument>>().InstancePerDependency();
            builder.RegisterType<JsonParser>().As<IParser<JObject>>().InstancePerDependency();

            return builder;
        }

        private static ContainerBuilder RegisterDao(this ContainerBuilder builder)
        {
            builder.RegisterType<XmlReader>().As<IFileReader>().InstancePerDependency();
            builder.RegisterType<JsonReader>().As<IFileReader>().InstancePerDependency();

            return builder;
        }

        private static ContainerBuilder RegisterLogic(this ContainerBuilder builder)
        {
            builder.RegisterType<CountLogic>().InstancePerDependency();
            builder.RegisterType<ManagerSettings>().As<IManagerSettings>().InstancePerDependency();
            builder.RegisterType<LogicManager>().SingleInstance();

            return builder;
        }
    }
}

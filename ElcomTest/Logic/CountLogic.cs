﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Dao;

namespace Logic
{
    public class CountLogic
    {
        public Dictionary<string, int> GetCountValue(IEnumerable<string> values)
        {
            if (values.IsNullOrEmpty())
            {
                return null;
            }

            var result = new Dictionary<string, int>();
            var groups = values.GroupBy(x => x);

            foreach (var group in groups)
            {
                result.Add(group.Key, group.Count());
            }

            return result;
        }
    }
}

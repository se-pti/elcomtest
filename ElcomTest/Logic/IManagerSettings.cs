﻿namespace Logic
{
    public interface IManagerSettings
    {
        int MaxThread { get; }
    }
}
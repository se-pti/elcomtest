﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Dao;

namespace Logic
{
    public class LogicManager
    {
        private readonly IManagerSettings _settings;
        private readonly IFileReader[] _readers;
        private readonly CountLogic _countLogic;

        public LogicManager(IManagerSettings settings, IFileReader[] readers, CountLogic countLogic)
        {
            _settings = settings;
            _readers = readers;
            _countLogic = countLogic;
        }

        public async Task<Dictionary<string, int>> GetValuesLengthAsync(string path, string rowName,
            bool isCollectionRow, CancellationToken cancellationToken = default)
        {
            var rows = new BlockingCollection<string>();
            var semaphore = new SemaphoreSlim(_settings.MaxThread, _settings.MaxThread);
            var tasks = new List<Task>();
            foreach (var reader in _readers)
            {
                cancellationToken.ThrowIfCancellationRequested();
                await semaphore.WaitAsync(cancellationToken);

                var task = ReadFiles(reader, rows, path, rowName, isCollectionRow, cancellationToken);
                task.ContinueWith(_ =>
                {
                    semaphore.Release();
                }, cancellationToken).ConfigureAwait(false);

                tasks.Add(task);
            }

            await Task.WhenAll(tasks);
            rows.CompleteAdding();

            var localResult = new List<string>();
            while (!rows.IsCompleted)
            {
                if (rows.TryTake(out var row))
                {
                    localResult.Add(row);
                }
            }

            return _countLogic.GetCountValue(localResult);
        }

        private Task ReadFiles(IFileReader reader, BlockingCollection<string> values, string path, string rowName,
            bool isCollectionRow, CancellationToken cancellationToken = default)
        {
            return Task.Run(async () =>
            {
                var paths = await reader.GetPaths(path, cancellationToken);
                var result = await reader.GetRowsFromFiles(paths, rowName, isCollectionRow, cancellationToken);

                if (result.IsNullOrEmpty())
                {
                    return;
                }

                foreach (var value in result)
                {
                    values.TryAdd(value);
                }
            });
        }
    }
}

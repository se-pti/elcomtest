﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class ManagerSettings : IManagerSettings
    {
        public int MaxThread => int.Parse(ConfigurationManager.AppSettings[nameof(MaxThread)]);
    }
}

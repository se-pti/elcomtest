﻿using AutoFixture;
using AutoFixture.NUnit3;

namespace TestProject.Fixtures
{
    public class AutoMoqRealAttribute : AutoDataAttribute
    {
        public AutoMoqRealAttribute() 
            : base(() => new Fixture().Customize(new RealFixturesCustomization()))
        {
            
        }
    }
}

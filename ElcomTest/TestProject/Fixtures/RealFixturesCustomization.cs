﻿using System.Xml;
using AutoFixture;
using AutoFixture.AutoMoq;
using Common;
using Dao;
using Newtonsoft.Json.Linq;
using XmlReader = Dao.XmlReader;

namespace TestProject.Fixtures
{
    public class RealFixturesCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize(new AutoMoqCustomization());

            fixture.Register<IParser<XmlDocument>>(() => new XmlParser());
            fixture.Register<IParser<JObject>>(() => new JsonParser());
            fixture.Register<IFileReader>(() => new XmlReader(fixture.Create<IParser<XmlDocument>>()));
            fixture.Register<IFileReader>(() => new JsonReader(fixture.Create<IParser<JObject>>()));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using TestProject.Fixtures;

namespace TestProject
{
    public class JsonTest
    {
        [Test, AutoMoqReal]
        public async Task ParseXml(JsonParser parser)
        {
            Assert.DoesNotThrow(() =>
            {
                var rowName = "Values";
                var json = FilesResource.Json1;
                var assertRows = new List<string>
                {
                    "a",
                    "12",
                    "4",
                    "12",
                    "q",
                    "12",
                    "84",
                    "w",
                    "q"
                };

                var doc = JObject.Parse(json);

                var rows = parser.FiendAllRowsInXmlDocByCollectionRows(doc, rowName);

                Assert.IsTrue(!assertRows.Except(rows).Any());
            });
        }
    }
}

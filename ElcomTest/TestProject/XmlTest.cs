﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using NUnit.Framework;
using System.Xml;
using TestProject.Fixtures;
using Assert = NUnit.Framework.Assert;

namespace TestProject
{
    public class XmlTest
    {
        [Test, AutoMoqReal]
        public async Task ParseXml(XmlParser parser)
        {
            Assert.DoesNotThrow(() =>
            {
                var rowName = "Values";
                var xml = FilesResource.Xml1;
                var assertRows = new List<string>
                {
                    "a",
                    "12",
                    "4",
                    "12",
                    "w",
                    "a",
                    "12",
                    "b",
                    "4",
                    "72"
                };

                var doc = new XmlDocument();
                doc.LoadXml(xml);

                var rows = parser.FiendAllRowsInXmlDocByCollectionRows(doc, rowName);

                Assert.IsTrue(!assertRows.Except(rows).Any());
            });
        }
    }
}
